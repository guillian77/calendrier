SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `eid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  PRIMARY KEY (`eid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `events` (`eid`, `name`, `start`, `end`) VALUES
(1, 'Hebdomadaire DF320', '2019-01-23 08:30:00', '2019-01-25 10:30:00'),
(2, 'Hebdomadaire CL31 06', '2019-01-23 08:30:00', '2019-01-23 10:30:00'),
(3, 'Hebdomadaire PWD22', '2019-01-23 08:30:00', '2019-01-23 10:30:00'),
(4, 'Hebdomadaire CL31 24', '2019-01-23 08:30:00', '2019-01-23 10:30:00'),
(5, 'Hebdomadaire TACAN', '2019-01-24 08:30:00', '2019-01-24 00:25:00'),
(6, 'Hebdomadaire PAR NG', '2019-01-24 08:30:00', '2019-01-24 00:25:00'),
(7, 'Export BDD serveur DIFF', '2019-01-26 08:30:00', '2019-01-26 09:00:00'),
(8, 'Bi-hebdomadaire ILS', '2019-01-21 08:30:00', '2019-01-21 12:00:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

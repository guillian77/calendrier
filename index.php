<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Calendrier</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>




<body>

    <!-- Navigation -->
    <nav class="navbar navbar-dark bg-primary mb-3">
        <a href="#" class="navbar-brand">ESICAéro: Planning</a>
    </nav>

    <?php
    require 'Month.php';
    require 'Events.php';

    $events = new Events();

    try {
        $month = new Month($_GET['month'] ?? null, $_GET['year'] ?? null, $_GET['week'] ?? null);
    } catch (\Throwable $th) {
        $month = new Month();
    }
    $start = $month->getStartingDay();
    $start = $start->format('N') === '1' ? $start : $month->getStartingDay()->modify('last monday');

    // Numéro de la semaine en cours
    $weekNumber = (clone $start)->format('W');
    // Nombre de semaine dans le mois précédent
    $weekInMonth = intval($month->getStartingDay()->modify('-1 day')->format('W')) - intval($month->getStartingDay()->modify('-1 month')->format('W')) + 1;

    $weeks = $month->getWeeks();
    $end = (clone $start)->modify('+' . (6 + 7 * ($weeks-1)) . ' days');
    
    $events = $events->getEventsBetweenByDay($start, $end);
    ?>

    <!-- Broadcum -->
    <div class="d-flex flex-row align-items-center justify-content-between mx-sm-3">
    <h1><?= $month->toString(); ?> <?php if(!empty($_GET['week'])): ?><span class="text-muted"> - Semaine <?= $_GET['week']+$weekNumber-1; ?></span><?php endif; ?></h1>
        <div>
            <span class="mr-1">Filtres:</span>
            <a href="#" class="btn btn-secondary">Planning personnel</a>
            <a href="#" class="btn btn-secondary">Tâches</a>
            <a href="#" class="btn btn-secondary mr-4">Maintenances</a>

            <span class="mr-1">Choix du mois:</span>
            <a href="?month=<?= $month->previousMonth()->month; ?>&year=<?= $month->previousMonth()->year; ?>" class="btn btn-primary">&lt;</a>
            <a href="?month=<?= $month->nextMonth()->month; ?>&year=<?= $month->nextMonth()->year; ?>" class="btn btn-primary">&gt;</a>
        </div>
    </div>

    <!-- Calendar -->
    <table class="calendar__table calendar__table--<?= $weeks; ?>weeks">
    <?php for($i = $month->getStartWeek(); $i < $month->getEndWeek(); $i++): ?>

        <!-- Bouton des semaines -->
        <?php if(empty($month->week)): ?>
            <span><a class="btn btn-primary ml-3 mb-3" href="?month=<?= $month->month; ?>&year=<?= $month->year; ?>&week=<?= $i+1; ?>">Semaine <?= $i+$weekNumber; ?></a>&nbsp;</span>
        <?php else: ?>
            <?php if($month->week > 1): ?>
                <a class="btn btn-primary mb-3 ml-3" href="?month=<?= $month->month; ?>&year=<?= $month->year; ?>&week=<?= $i; ?>">&lt;</a>
            <?php else: ?>
                <a class="btn btn-primary mb-3 ml-3" href="?month=<?= $month->month-1; ?>&year=<?= $month->year; ?>&week=<?= $weekInMonth; ?>">&lt;</a>
            <?php endif; ?>

            <a class="btn btn-primary mb-3 ml-3" href="?month=<?= $month->month; ?>&year=<?= $month->year; ?>">Afficher le mois</a>
            
            <?php if($month->week < $month->getWeeks()): ?>
                <a class="btn btn-primary mb-3 ml-3" href="?month=<?= $month->month; ?>&year=<?= $month->year; ?>&week=<?= $i+2; ?>">&gt;</a>
            <?php else: ?>
                <a class="btn btn-primary mb-3 ml-3" href="?month=<?= $month->month+1; ?>&year=<?= $month->year; ?>&week=1">&gt;</a>
            <?php endif; ?>            
        <?php endif; ?>
        
        <tr>
            <?php foreach($month->days as $k => $day):
                $date = (clone $start)->modify("+" . ($k + $i * 7) . "days");
                $eventForDay = $events[$date->format('Y-m-d')] ?? [];
                ?>
                <td class="<?= $month->whithinMonth($date) ? '' : 'calendar__othermonth'; ?>">
                    <!-- Jours en lettre -->
                    <?php if($i === 0): ?>
                        <div class="calendar__weekday"><?= $day; ?></div>
                    <?php endif; ?>

                    <!-- Numéro du jours -->
                    <div class="calendar__day">
                        <?= $date->format('d'); ?>
                    </div>

                    <!-- Events -->
                    <?php foreach($eventForDay as $event): ?>
                        <div class="calendar__events">
                            <!-- https://www.cirrusinsight.com/support/gmail/what-is-the-hex-color-code-legend-for-googles-calendar -->
                            
                            <span class="event" <?= ($event['color']) ? 'style="background-color:'.$event['color'].';' : ''; ?>">
                                <?= (new DateTime($event['start']))->format('H\hi'); ?> <a href="#"><?= $event['name']; ?></a>
                            </span>
                        </div>
                    <?php endforeach; ?>

                </td>
            <?php endforeach; ?>
        </tr>
        <?php endfor; ?>
    </table>

    <!-- BOUTON: Ajout de tâche -->
    <a class="calendar__button" href="#">+</a>


</body>
</html>
<?php

class Month {

    private $months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    public $days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

    public $month;
    public $year;
    public $week;
    
    /**
     * MONTH CONSTRUCTOR
     *
     * @param integer $month Le mois compris entre 1 et 12
     * @param integer $year L'année
     * @throws Exception
     */
    public function __construct(?int $month = null, ?int $year = null, ?int $week = null) {
        // Définir la date actuelle si rien est spécifié
        if ($month === null || $month < 1 || $month > 12) {
            $month = intval(date('m'));
        }

        if ($year === null) {
            $year = intval(date('Y'));
        }

        // Vérifier le format de la date entré
        if ($month < 1 || $month > 12) {
            throw new Exception("Le mois $month n'est pas valide");
        }
        
        if ($year < 1970) {
            throw new Exception("L'année est inférieur à 1970");
        }

        // Définir les propriétées
        $this->month = $month;
        $this->year = $year;
        $this->week = $week;
    }

    /**
     * TO STRING
     * 
     * Retourne le mois en toute lettre (Ex: Mars 2018)
     *
     * @return string
     */
    public function toString(): string {
        return $this->months[$this->month - 1] .  ' ' . $this->year;
    }

    /**
     * GET STRATING DAY
     *
     * Renvoie le premier jours du mois
     * 
     * @return DateTime
     */
    public function getStartingDay(): DateTime {
        return new DateTime("{$this->year}-{$this->month}-01");
    }

    /**
     * GET WEEKS
     * 
     * Retourne le nombre de semaine dans le mois.
     *
     * @return integer
     */
    public function getWeeks(): int {
        $start = $this->getStartingDay();
        $end = (clone $start)->modify('+1 month -1 day');
        
        $weeks = intval($end->format('W')) - intval($start->format('W')) + 1;

        if ($weeks < 0) {
            $weeks = 5;
        }

        return $weeks;
    }

    /**
     * WHITHIN MONTH
     * 
     * Est-cd que le jour est dans le mois ?
     *
     * @return boolean
     */
    public function whithinMonth(DateTime $date): bool {
        return $this->getStartingDay()->format('Y-m') === $date->format('Y-m');
    }

    /**
     * NEXT MONTH
     * 
     * Renvoie le mois suivant.
     *
     * @return Month
     */
    public function nextMonth(): Month {
        $month = $this->month + 1;
        $year = $this->year;
        if ($month > 12) {
            $month = 1;
            $year += 1;
        }

        return new Month($month, $year);
    }

    /**
     * PREVIOUS MONTH
     * 
     * Renvoie le mois précédent.
     *
     * @return Month
     */
    public function previousMonth(): Month {
        $month = $this->month - 1;
        $year = $this->year;
        if ($month < 1) {
            $month = 12;
            $year -= 1;
        }

        return new Month($month, $year);
    }

    /**
     * GET START WEEK
     * 
     * Récupère la semaine de départ.
     * 
     * @return Week
     */
    public function getStartWeek(): int {
        if($this->week === null || $this->week < 1) {
            return $this->week = 0;
        }
        return $this->week-1;
    }

    /**
     * GET END WEEK
     * 
     * Récupère la semaine de fin.
     * 
     * @return Week
     */
    public function getEndWeek(): int {
        if($this->week === null || $this->week < 1) {
            return $this->getWeeks();
        }
        return $this->week;
    }
}

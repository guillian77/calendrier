<?php

class Events
{
    /**
     * GET EVENTS BETWEEN
     * 
     * Permet de récupérer les events situés entre une date de début et de fin.
     * 
     * @return Array
     */
    public function getEventsBetween(DateTime $start, DateTime $end): array {
        $db = new PDO('mysql: host=localhost; dbname=esic', 'root', '', [
            'PDO::ATTR_DEUFAULT_FETCH_MODE => PDO::FETCH_ASSOC',
            'PDO::ATTR_ERRMODE => PDO::ERRMODE_EXEPTION'
        ]);

        $sql = "SELECT * FROM events WHERE start BETWEEN '{$start->format('Y-m-d 00:00:00')}' AND '{$end->format('Y-m-d 23:59:59')}'";

        $stmt = $db->query($sql);
        $results = $stmt->fetchAll();
        return $results;
    }

    /**
     * GET EVENTS BETWEEN BY DAY
     * 
     * Récupérer les events .. Date de départ,fin par jours.
     * 
     * @return Array
     */
    public function getEventsBetweenByDay(DateTime $start, DateTime $end): array {
        $events = $this->getEventsBetween($start, $end);

        $days = [];

        foreach($events as $event) {
            $date = explode(' ', $event['start'])[0];

            if (!isset($days[$date])) {
                $days[$date] = [$event];
            } else {
                $days[$date][]= $event;
            }
        }
        return $days;
    }
}
